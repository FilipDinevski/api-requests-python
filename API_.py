import requests


url = 'https://api.github.com/search/repositories?q=language:python&sort=stars'
r = requests.get(url)
print(r.status_code)

response_dict = r.json()
print(response_dict.keys())

print("total repository:", response_dict["total_count"])

repo_dicts = response_dict["items"]
print("total returned:", len(repo_dicts))

repo_dict = repo_dicts[0]
print("\nKeys", repo_dict.keys())

for key in sorted(repo_dict.keys()):
    print(key)

for repo_dict in repo_dicts:
    print(repo_dict["name"])
    print(repo_dict["owner"]['login'])
    print(repo_dict['stargazers_count'])

 


